from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def home(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=request.user
        )
        form.fields["account"].queryset = Account.objects.filter(
            owner=request.user
        )
    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def my_receipt_list(request):
    my_receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "my_receipt": my_receipt,
    }
    return render(request, "receipts/home.html", context)


def category_list(request):
    category = ExpenseCategory.objects.all()
    context = {"category": category}
    return render(request, "receipts/categories.html", context)


def account_list(request):
    account = Account.objects.all()
    context = {"account": account}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_categories.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_account.html", context)
